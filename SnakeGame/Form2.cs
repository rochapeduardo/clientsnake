﻿using SnakeGame.Service.WebService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SnakeGame
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnRegistro_Click(object sender, EventArgs e)
        {
            WebService wSJogador = new WebService();

            try
            {
                wSJogador.Register(new Jogador{
                    Nick = txtNick.Text,
                    Senha = txtSenha.Text,
                    Mail = txtMail.Text
                });

                MessageBox.Show("Cadastro efetuado. Faça seu login...");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Não foi possível registrar");

            }
        }
    }
}
