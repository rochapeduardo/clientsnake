﻿using SnakeGame.Service.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnakeGame
{
    class Settings
    {
        public static int Width { get; set; }
        public static int Height { get; set; }
        
        public Settings()
        {
            Width = 16;
            Height = 16;
        }
    }

    public enum Direction
    { 
        Up,
        Down,
        Left,
        Right   
    };
    
    public class Player
    {
        public int Cod { get; set; }
        public int Speed { get; set; }
        public int Score { get; set; }
        public int Points { get; set; }
        public bool GameOver { get; set; }
        public bool Pegou { get; set; }
        public Direction direction { get; set; }
        public List<Circle> Snake = new List<Circle>();

        public List<Historico> pws;
        public int appleCount = 0, appleX = 0, appleY = 0;
        public Historico peguei = null;

        public Player(int X = 10, int Y = 5, int speed = 1, int points = 100, Direction d = Direction.Down)
        {
            this.Speed = speed;
            this.Score = 0;
            this.Points = points;
            this.GameOver = false;
            this.direction = d;
            Pegou = false;
            Circle head = new Circle{X = 10, Y = 5 };
            Snake.Add(head);
        }
    }
}
