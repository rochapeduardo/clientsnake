﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;
using SnakeGame.Service.WebService;

namespace SnakeGame
{
    public partial class FrmGame : Form
    {
        private Circle food = new Circle();

        bool inGame = false;
        Player p1, p2;
        Historico h = null;

        int maxX=0, maxY=0, pUpX,pUpY,currentPUp;
        TcpClient cliente;
        StreamReader reader = null;
        StreamWriter writer = null;
        String servidor = "127.0.0.1";

        Thread thread;

        private void conectar()
        {
            try
            {
                int porta = 5000;
                cliente = new TcpClient(servidor, porta);
                NetworkStream stream = cliente.GetStream();

                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                
                try
                {
                    String dados = reader.ReadLine();
                    Console.WriteLine("Dado: " + dados); 
                    if (dados == "Pronto")
                    {
                        enviar(p1.Cod.ToString() + "/" + maxY.ToString() + "/" + maxX.ToString());
                    }
                    dados = reader.ReadLine();
                    Console.WriteLine("Dado: " + dados);
                    if (dados == "StartGame")
                        StartGame();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro no cliente: {0}", e);
                }
            }
            catch (Exception e)
            {   
                //TODO: Conferir o tipo de exception para evitar deadlock
                //Todo: fazer via web service
                System.Diagnostics.Process.Start("C:\\Users\\felli\\Documents\\Faculdade\\Faculdade2\\SnakeGame\\Rede.Servidor\\bin\\Debug\\Rede.Servidor.exe");
                try
                {
                    conectar();
                }
                catch
                {
                    Console.WriteLine("Erro no cliente: {0}", e);
                }
            }
        }

        private void listenning()
        {
            try
            {
                String dados = reader.ReadLine();

                while (dados != null)
                {
                    try
                    {
                        p2 = JsonConvert.DeserializeObject<Player>(dados);
                    }
                    catch
                    {
                        try
                        {
                            p1.pws = JsonConvert.DeserializeObject<List<Historico>>(dados);
                        }
                        catch
                        {
                            int a = 0;

                            a = dados.IndexOf("/");
                            if (dados.Substring(0, a) == "peguei o item")
                            {
                                dados = dados.Substring(a + 1);
                                Historico hTemp = JsonConvert.DeserializeObject<Historico>(dados);
                                if (hTemp == h)
                                {
                                    h = null;
                                    Console.WriteLine("O powerUp ja foi coletado...");
                                }
                            }
                            else
                            {
                                Console.WriteLine("a string passada é: " + dados);
                                a = 0;

                                a = dados.IndexOf("/");
                                pUpX = Int32.Parse(dados.Substring(0, a));
                                Console.WriteLine("X é: " + maxX.ToString());
                                dados = dados.Substring(a + 1);
                                Console.WriteLine("a string passada é: " + dados);

                                a = dados.IndexOf("/");
                                pUpY = Int32.Parse(dados.Substring(0, a));
                                Console.WriteLine("Y é: " + maxY.ToString());
                                dados = dados.Substring(a + 1);
                                Console.WriteLine("a string passada é: " + dados);

                                a = dados.IndexOf("/");
                                currentPUp = Int32.Parse(dados.Substring(0, a));
                                Console.WriteLine("currentPUp é: " + currentPUp.ToString());
                                dados = dados.Substring(a + 1);
                                Console.WriteLine("a string passada é: " + dados);

                                Console.WriteLine("final---------------------");

                                h = JsonConvert.DeserializeObject<Historico>(dados);
                            }
                        }
                    }

                    dados = reader.ReadLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Erro no cliente: {0}", e);
            }
        }
        
        #region enviar
        public void enviar(Player player)
        {
            string json = JsonConvert.SerializeObject(player);
            writer.WriteLine(json);
            writer.Flush();
        }

        public void enviar(int s)
        {
            writer.WriteLine(s.ToString());
            writer.Flush();
        }

        public void enviar(string s)
        {
            writer.WriteLine(s);
            writer.Flush();
        }

        public void enviar(string s, Historico h)
        {
            string json = s+ JsonConvert.SerializeObject(h);
            writer.WriteLine(json);
            writer.Flush();
        }
        #endregion
        
        public FrmGame()
        {
            InitializeComponent();

            // Set settings to default;
            p1 = new Player();
            p2 = new Player();
            //Set game speed and start timer
            gameTimer.Interval = 1000 / p1.Speed;
            gameTimer.Tick += UpdateScreen;
            gameTimer.Start();

            new Settings();
            maxX = pbCanvas.Size.Width / Settings.Width;
            maxY = pbCanvas.Size.Height / Settings.Height;


        }

        public void StartGame()
        {
            Thread listen = new Thread(listenning);
            listen.Start();
            inGame = true;
            
            lblGameOver.Visible = false;
            
            lblScore.Text = p1.Score.ToString();
            lblScore2.Text = p2.Score.ToString();

           // GenerateFood();

        }
        
        private void MovePlayer()
        {
            if (!inGame)
                return;

            for (int i = p1.Snake.Count - 1; i >= 0; i--)
            {
                //Move head
                if (i == 0)
                {
                    switch (p1.direction)
                    {
                        case Direction.Right:
                            p1.Snake[i].X++;
                            break;
                        case Direction.Left:
                            p1.Snake[i].X--;
                            break;
                        case Direction.Up:
                            p1.Snake[i].Y--;
                            break;
                        case Direction.Down:
                            p1.Snake[i].Y++;
                            break;
                    }
                    
                    //Detect collision with game borders
                    if (p1.Snake[i].X < 0 || p1.Snake[i].Y < 0 || p1.Snake[i].X >= maxX || p1.Snake[i].Y >= maxY)
                        Die();

                    //Detect collisions with body
                    for (int j = 1; j < p1.Snake.Count; j++)
                    {
                        if (p1.Snake[i].X == p1.Snake[j].X && p1.Snake[i].Y == p1.Snake[j].Y)
                        {
                            Die();
                        }
                    }

                    if (p1.pws != null && p1.appleCount > 0 && p1.pws[p1.appleCount] != null)
                    {
                       if (p1.Snake[0].X == p1.appleX && p1.Snake[0].Y == p1.appleY)
                       {
                            Eat(p1.pws[p1.appleCount]);
                       }

                    }
                    //Detect colission with food piece

                    //Detect colission with Power Up piece

                    if (h != null)
                    {
                        if (p1.Snake[0].X == pUpX && p1.Snake[0].Y == pUpY)
                        {
                            pUpX = -1;
                            pUpY = -1;
                            Eat(h);
                        }


                    }
                }
                else
                {
                    //Move body
                    p1.Snake[i].X = p1.Snake[i - 1].X;
                    p1.Snake[i].Y = p1.Snake[i - 1].Y;
                }
            }
        }
       
        private void UpdateScreen(object sender, EventArgs e) 
        {
            if (!inGame)
                return;


            pbCanvas.Invalidate();
            pbCanvas2.Invalidate();
            lblScore2.Text = p2.Score.ToString();

            //Check for Game Over
            if (p1.GameOver)
            {
                if (Input.KeyPressed(Keys.Enter))
                {
                    this.Close();
                }

            }
            else            
            {
                if (Input.KeyPressed(Keys.Right) && p1.direction != Direction.Left)
                    p1.direction = Direction.Right;
                else if (Input.KeyPressed(Keys.Left) && p1.direction != Direction.Right)
                    p1.direction = Direction.Left;
                else if (Input.KeyPressed(Keys.Up) && p1.direction != Direction.Down)
                    p1.direction = Direction.Up;
                else if (Input.KeyPressed(Keys.Down) && p1.direction != Direction.Up)
                    p1.direction = Direction.Down;

                MovePlayer();

                enviar(p1);
                p1.Pegou = false;
            }
            
            
        }


        private void GenerateFood()
        {
            Random random = new Random();
            p1.appleX = random.Next(maxX);
            p1.appleY = random.Next(maxY);

        }

        private void Die()
        {
            if (!inGame)
                return;

            p1.GameOver = true;
        }

        private void Eat(Historico historico)
        {
            if (!inGame)
                return;
            p1.peguei = historico;

            if (historico.Item.Tipo.Id_Tipo == 1)
            {//Add circle to body

                //enviar("peguei o item/", historico);
                p1.Snake.Add(new Circle
                {
                    X = p1.Snake[p1.Snake.Count - 1].X,
                    Y = p1.Snake[p1.Snake.Count - 1].Y
                });
                p1.appleCount++;
                GenerateFood();
            }
            else if (historico.Item.Tipo.Id_Tipo == 1)
            {//Add circle to body
                //p1.Pegou = true;
                p1.Snake.Add(new Circle
                {
                    X = p1.Snake[p1.Snake.Count - 1].X,
                    Y = p1.Snake[p1.Snake.Count - 1].Y
                });
                p1.Snake.Add(new Circle
                {
                    X = p1.Snake[p1.Snake.Count - 1].X,
                    Y = p1.Snake[p1.Snake.Count - 1].Y
                });
            }
            else if (historico.Item.Id_Item == 21)
            {
                //p1.Pegou = true;
                thread = new Thread(DoublePoints);
                thread.Start();
            }
            //Update score
            p1.Score += historico.Item.Tipo.Valor * p1.Points;
            lblScore.Text = p1.Score.ToString();
        }

        private void DoublePoints()
        {
            DateTime start = DateTime.Now;
            int duration = 15;
            p1.Points *= 2;
            Thread.Sleep(duration * 1000);
            p1.Points /= 2;
            thread.Abort();

        }
        

        private void pbCanvas_Paint(object sender, PaintEventArgs e)
        {
            if (!inGame)
                return;
            Graphics canvas = e.Graphics;

            if (!p1.GameOver)
            {
                //Set color of snake

                //Draw Snake
                for (int i = 0; i < p1.Snake.Count; i++)
                {
                    Image img;
                    var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                    string root = path.Substring(6) + "\\Images\\";
                    if (i == 0)
                    {
                        img = Image.FromFile(root + "imgCabeca.png");
                    }
                    else if (i == p1.Snake.Count - 1)
                    {
                        img = Image.FromFile(root + "imgPonta.png");
                    }
                    else
                    {
                        img = Image.FromFile(root + "imgCorpo.png");
                    }

                    canvas.DrawImage(img, new Rectangle(p1.Snake[i].X * Settings.Width,
                                      p1.Snake[i].Y * Settings.Height,
                                      Settings.Width, Settings.Height));

                    if(p1.pws != null && p1.appleCount > 0 && p1.pws[p1.appleCount] != null)
                    {
                        Image itemImg = Image.FromFile(root + p1.pws[p1.appleCount].Item.Imagem);

                        canvas.DrawImage(itemImg, new Rectangle(p1.appleX * Settings.Width, p1.appleY * Settings.Height, Settings.Width, Settings.Height));
                    }


                    //-------------power up
                    if (h != null)
                    {
                        Console.WriteLine(h.Item.Nome + "----------------------------------------------------------------------");
                        Image pUpImg = Image.FromFile(root + h.Item.Imagem);
                        canvas.DrawImage(pUpImg,
                         new Rectangle(pUpX * Settings.Width,
                             pUpY * Settings.Height, Settings.Width, Settings.Height));
                    }
                }
            }
            else
            {
                lblGameOver.Text = "Game over  \nYour final score is: \n" + p1.Score + "\nPress Enter to try again";
                lblGameOver.Visible = true;

                if (p2.GameOver)
                {
                    if (p1.Score > p2.Score)
                        lblGameOver.Text = "WIN!! \nPress Enter to disconnect...";
                    else if (p1.Score < p2.Score)
                        lblGameOver.Text = "LOSE!! \nPress Enter to disconnect...";
                    else
                        lblGameOver.Text = "Draw!!! Nice game! \nPress Enter to disconnect...";
                    if (Input.KeyPressed(Keys.Enter))
                    {
                        this.Close();
                    }
                }
            }

        }

        private void pbCanvas2_Paint(object sender, PaintEventArgs e)
        {
            if (!inGame)
                return;
            Graphics canvas = e.Graphics;

            if (!p2.GameOver)
            {
                //Set color of snake

                //Draw Snake
                for (int i = 0; i < p2.Snake.Count; i++)
                {
                    Image img;
                    var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                    string root = path.Substring(6) + "\\Images\\";
                    if (i == 0)
                    {
                        img = Image.FromFile(root + "imgCabeca.png");
                    }
                    else if (i == p2.Snake.Count - 1)
                    {
                        img = Image.FromFile(root + "imgPonta.png");
                    }
                    else
                    {
                        img = Image.FromFile(root + "imgCorpo.png");
                    }

                    canvas.DrawImage(img, new Rectangle(p2.Snake[i].X * Settings.Width,
                                      p2.Snake[i].Y * Settings.Height,
                                      Settings.Width, Settings.Height));

                    if (p2.pws != null && p2.appleCount > 0 && p2.pws[p2.appleCount] != null)
                    {
                        Image itemImg = Image.FromFile(root + p2.pws[p2.appleCount].Item.Imagem);
                        canvas.DrawImage(itemImg, new Rectangle(p2.appleX * Settings.Width, p2.appleY * Settings.Height, Settings.Width, Settings.Height));

                    }


                    if (h != null)
                    {
                        Console.WriteLine(h.Item.Nome+ "----------------------------------------------------------------------");
                        Image pUpImg = Image.FromFile(root + h.Item.Imagem);
                        canvas.DrawImage(pUpImg,
                         new Rectangle(pUpX * Settings.Width,
                             pUpY * Settings.Height, Settings.Width, Settings.Height));
                    }
                }
            }
            else
            {
                lblGameOver2.Text = "Game over  \nYour opponent final score is: \n" + p2.Score + "\nPress Enter to try again";
                lblGameOver2.Visible = true;

                if (p1.GameOver)
                {
                    if (p1.Score > p2.Score)
                        lblGameOver2.Text = "LOSE!! \nPress Enter to disconnect...";
                    else if (p1.Score < p2.Score)
                        lblGameOver2.Text = "WIN!! \nPress Enter to disconnect...";
                    else
                        lblGameOver2.Text = "Draw!!! Nice game! \nPress Enter to disconnect...";
                    if (Input.KeyPressed(Keys.Enter))
                    {
                        this.Close();
                    }
                }
            }

        }
        

        private void Logar()
        {
            WebService WJogador = new WebService();

            Jogador jogador = WJogador.Login(txtNick.Text, txtSenha.Text);

            if (jogador != null)
            {
                p1.Cod = jogador.Id_Jogador;
                btnConectar.Enabled = false;
                txtNick.Enabled = false;
                txtSenha.Enabled = false;
                btnRegistro.Enabled = false;
                conectar();
            }
            else
            {
                MessageBox.Show("Os dados de acesso não conferem!!");
            }
        }

        private void Registro()
        {
            Form2 f = new Form2();
            f.ShowDialog();

        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, true);
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            Logar();
        }
        
        private void btnRegistro_Click(object sender, EventArgs e)
        {
            Registro();
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            Input.ChangeState(e.KeyCode, false);
        }
        
    }
}
