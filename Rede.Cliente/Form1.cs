﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Rede.Cliente
{
    public partial class FrmPrincipal : Form
    {
        TcpClient cliente;
        StreamReader reader = null;
        StreamWriter writer = null;
        String servidor = "127.0.0.1";

        int cont = 0;

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void cmdConectar_Click(object sender, EventArgs e)
        {
            conectar();
        }

        private void conectar()
        {
            try
            {
                int porta = 5000;
                cliente = new TcpClient(servidor, porta);
                NetworkStream stream = cliente.GetStream();

                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);

                Thread thread = new Thread(listenning);
                thread.Start();

            }catch(Exception e)
            {
                Console.WriteLine("Erro no cliente: {0}", e);
            }
        }

        private void listenning()
        {
            try
            {
                String dados = reader.ReadLine();

                while (dados != null)
                {
                    lblNumero.Text = dados;
                    dados = reader.ReadLine();
                }

            } catch (Exception e)
            {
                Console.WriteLine("Erro no cliente: {0}", e);
            }
        }

        public void enviar (String mensagem)
        {
            writer.WriteLine(mensagem);
            writer.Flush();
        }

        private void cmdGerarNumero_Click(object sender, EventArgs e)
        {
            this.cont++;
            enviar("" + this.cont);
        }

        private void cmdLimpar_Click(object sender, EventArgs e)
        {
            lblNumero.Text = "0";
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {

        }
    }
}
