﻿namespace Rede.Cliente
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdLimpar = new System.Windows.Forms.Button();
            this.lblNumeroGerado = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.cmdGerarNumero = new System.Windows.Forms.Button();
            this.cmdConectar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmdLimpar
            // 
            this.cmdLimpar.Location = new System.Drawing.Point(12, 174);
            this.cmdLimpar.Name = "cmdLimpar";
            this.cmdLimpar.Size = new System.Drawing.Size(209, 23);
            this.cmdLimpar.TabIndex = 0;
            this.cmdLimpar.Text = "Limpar";
            this.cmdLimpar.UseVisualStyleBackColor = true;
            this.cmdLimpar.Click += new System.EventHandler(this.cmdLimpar_Click);
            // 
            // lblNumeroGerado
            // 
            this.lblNumeroGerado.AutoSize = true;
            this.lblNumeroGerado.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroGerado.Location = new System.Drawing.Point(30, 18);
            this.lblNumeroGerado.Name = "lblNumeroGerado";
            this.lblNumeroGerado.Size = new System.Drawing.Size(153, 25);
            this.lblNumeroGerado.TabIndex = 1;
            this.lblNumeroGerado.Text = "Número gerado:";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Enabled = false;
            this.lblNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.Location = new System.Drawing.Point(27, 49);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(42, 46);
            this.lblNumero.TabIndex = 2;
            this.lblNumero.Text = "0";
            // 
            // cmdGerarNumero
            // 
            this.cmdGerarNumero.Location = new System.Drawing.Point(12, 136);
            this.cmdGerarNumero.Name = "cmdGerarNumero";
            this.cmdGerarNumero.Size = new System.Drawing.Size(209, 23);
            this.cmdGerarNumero.TabIndex = 3;
            this.cmdGerarNumero.Text = "Gerar Número";
            this.cmdGerarNumero.UseVisualStyleBackColor = true;
            this.cmdGerarNumero.Click += new System.EventHandler(this.cmdGerarNumero_Click);
            // 
            // cmdConectar
            // 
            this.cmdConectar.Location = new System.Drawing.Point(12, 98);
            this.cmdConectar.Name = "cmdConectar";
            this.cmdConectar.Size = new System.Drawing.Size(209, 23);
            this.cmdConectar.TabIndex = 4;
            this.cmdConectar.Text = "Conectar";
            this.cmdConectar.UseVisualStyleBackColor = true;
            this.cmdConectar.Click += new System.EventHandler(this.cmdConectar_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 217);
            this.Controls.Add(this.cmdConectar);
            this.Controls.Add(this.cmdGerarNumero);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.lblNumeroGerado);
            this.Controls.Add(this.cmdLimpar);
            this.Name = "FrmPrincipal";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdLimpar;
        private System.Windows.Forms.Label lblNumeroGerado;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Button cmdGerarNumero;
        private System.Windows.Forms.Button cmdConectar;
    }
}

