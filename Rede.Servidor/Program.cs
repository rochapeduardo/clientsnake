﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;


namespace Rede.Servidor
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener listener = null;
            int porta = 5000;
            ControleJogo controle = new ControleJogo();
            bool rodando = true;

            try
            {
                IPAddress enderecoServidor = IPAddress.Parse("127.0.0.1");
                listener = new TcpListener(enderecoServidor, porta);
                listener.Start();

                while (rodando)
                {
                    if (!controle.estaPronto())
                    {
                        Console.WriteLine("Aguardando conexões.");
                        TcpClient cliente = listener.AcceptTcpClient();
                        controle.adicionaJogador(cliente);
                    }
                    
                }

            }catch (SocketException se)
            {
                Console.WriteLine("Erro de rede: {0}", se);
            }
        }
    }
}
