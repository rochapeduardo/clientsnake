﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using SnakeGame;

using Newtonsoft.Json;
using Rede.Servidor.Service.WebService;

namespace Rede.Servidor
{
    class ControleJogo
    {
        const int MAXIMO = 2;
        int id = 0;
        bool pronto, startGame, aguardando;

        public Jogador[] jogadores;

        Partida partidaAtual;

        public List<Historico> historicosPowerUps = new List<Historico>(), velocidade = new List<Historico>();
        public int listPos = 0;

        DateTime inicio, contador;

        int maxX = 0, maxY = 0;
        int delayPUp = 5;
        bool pUpActive;


        public bool estaPronto()
        {
            return this.pronto;
        }

        public ControleJogo ()
        {
            this.jogadores = new Jogador[MAXIMO];
        }

        private int proximoId()
        {
            return this.id++;
        }
        
        public void adicionaJogador(TcpClient cliente)
        {
            if (this.id > MAXIMO - 1)
            {
                desconectarJogador(cliente);
                return;
            }

            int id = proximoId();
            jogadores[id] = new Jogador(this, id, cliente);
            Console.WriteLine("Novo jogador conectado com o id: " + jogadores[id].id);

            if (id != MAXIMO - 1)
                return;
            Console.WriteLine("O servidor tem dois jogadores!");

            for (int i = 0; i < jogadores.Length; i++)
            {
                StreamReader reader = new StreamReader(jogadores[id].cliente.GetStream());
                jogadores[i].envia("Pronto");
            }

            int max = 200;

            Console.WriteLine("Esperando os jogadores ficarem prontos");
            while ((jogadores[0].count == 0 && jogadores[1].count == 0) && max!=0)
            {
                Console.WriteLine(max);
                max--;
            }
            if(max == 0)
            {
                Console.WriteLine("Não foi possível inserir as informações dos jogadores a tempo");
                //TODO: desconectar jogadores
                return;
            }
            Console.WriteLine("As informações do jogadores foram inseridas com sucesso!!");

            if (jogadores[0].maxX != jogadores[1].maxX || jogadores[0].maxY != jogadores[1].maxY)
            {
                Console.WriteLine("Há um divergencia no tamanho das telas dos jogadores");
                //TODO: desconectar jogadores
                //return;
            }
            maxX = jogadores[0].maxX;
            maxY = jogadores[0].maxY;

            Console.WriteLine("partida: " + jogadores[0].Cod + " | " + jogadores[1].Cod);
            WebService wSJogador = new WebService();
            Partida partida = new WebService().CriarPartida(jogadores[0].Cod, jogadores[1].Cod);

            if (partida == null)
            {
                Console.WriteLine("Ocorreu Algum problema ao inserir a partida no banco de dados... ");
                //TODO: desconectar jogadores
                return;
            }
            
            Console.WriteLine("o codigo da partida é: " + partida.Id_Partida);
            partidaAtual = partida;

            List<Historico> temp = null;
            //Console.WriteLine(wSJogador.listarHistorico(partidaAtual)[0]);
            try
            {
                temp = new WebService().ListarHistorico(partidaAtual.Id_Partida).ToList<Historico>();
                if (temp != null)
                    Console.WriteLine("A lista de itens foi coletada");
                else
                    Console.WriteLine("A listagem itens falhou : lista null");
            }catch
            {
                Console.WriteLine("A listagem itens falhou: Exception");
            }

            try
            {
                Console.WriteLine("Adicionando itens as suas respectivas listas (apples e historicosPowerUps)");
                for (int i = 0; i < temp.Count; i++)
                {
                    if (temp[i].Item.Tipo.Id_Tipo == 1)
                    {
                        velocidade.Add(temp[i]);
                    }
                    else
                    {
                        historicosPowerUps.Add(temp[i]);
                    }
                }
                Console.WriteLine("adiconou nas listas");

            }
            catch
            {
                //TODO: desconectar jogadores
                Console.WriteLine("Falha ao adicionar os itens as suas respectivas listas...");
                return;
            }


            for (int i = 0; i < jogadores.Length; i++)
            {
                Console.WriteLine("Start Game");
                jogadores[i].envia("StartGame");
                startGame = true;

            }
            for (int i = 0; i < jogadores.Length; i++)
            {
                jogadores[i].envia(JsonConvert.SerializeObject(velocidade));
                Console.WriteLine("enviando as apples para o jogador de id: " + jogadores[i].id);
            }

            inicio = DateTime.Now;
            Start();

        }

        public void Start()
        {
            int cont = 0;
            contador = DateTime.Now;


            while (startGame)//update
            {
                if (cont == 0)
                {
                    if (jogadores[0].gameOver && jogadores[1].gameOver)
                    {
                        Console.WriteLine("Servidor--GameOver");
                        endGame(partidaAtual, inicio);
                        cont++;

                    }
                }

                if ((DateTime.Now - contador).TotalSeconds >= delayPUp)
                {
                    Console.WriteLine("tentou enviar um power up");
                    contador = DateTime.Now;
                    if (!pUpActive)
                    {
                        GeneratePowerUp();
                        Console.WriteLine("conseguiu");
                    }
                    else
                    {
                        pUpActive = false;
                        //tirar ele da tela
                        delayPUp = 5;
                    }
                }
                
            }
            
        }

        private void desconectarJogador(TcpClient cliente)
        {
            NetworkStream stream = cliente.GetStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.WriteLine("Servidor lotado!");
            Console.WriteLine("Nova tentativa de conexão com o servidor lotado.");
            writer.Flush();
            writer.Close();
        }

        private void GeneratePowerUp()
        {
            if (historicosPowerUps[listPos].Item.Tipo.Tempo.ToString() == "0")
            {
                Console.WriteLine("Veio o item errado");
                listPos++;
                return;
            }
            int x = 0, y = 0;
            Random random = new Random();
            x = random.Next(0, maxX);
            y = random.Next(0, maxY);

            for (int i = 0; i < jogadores.Length; i++)
            {
                jogadores[i].envia(historicosPowerUps[listPos], x, y, listPos);
            }

            delayPUp = Int32.Parse(historicosPowerUps[listPos].Item.Tipo.Tempo.ToString());
            pUpActive = true;
            Console.WriteLine("Enviando novo powerUp...");
        }

        private System.TimeSpan duration(DateTime start)
        {
            return (DateTime.Now - inicio);
        }

        private void endGame(Partida p, DateTime start)
        {
            Console.WriteLine((int)(duration(start).TotalSeconds));
            WebService wSJogador = new WebService();
            if (wSJogador.AlterarTempoPartida(p, duration(start).TotalSeconds))
                Console.WriteLine("Tempo Final de partida alterado com sucesso!");
            else
                Console.WriteLine("Não foi possível alterar o tempo final de partida...");

        }

        public void pegouItem(int listPosicao, int idJ)
        {
            WebService wSJogador = new WebService();

            if (idJ == 0)
            {
                if (wSJogador.AlterarHistorico(historicosPowerUps[listPosicao], partidaAtual.Jogador1, idJ))
                {
                    Console.WriteLine("Alteração de histórico do power Up coletado pelo jogador " + idJ + " efetuada com sucesso.");
                    historicosPowerUps[listPosicao].Jogador1 = partidaAtual.Jogador1;
                }
                else
                    Console.WriteLine("Falha na alteração de histórico do power Up coletado pelo jogador " + idJ);
            }
            else if (idJ == 1)
            {
                Console.WriteLine(wSJogador.AlterarHistorico(historicosPowerUps[listPosicao], partidaAtual.Jogador2, idJ));
                if (wSJogador.AlterarHistorico(historicosPowerUps[listPosicao], partidaAtual.Jogador2, idJ))
                {
                    Console.WriteLine("Alteração de histórico do power Up coletado pelo jogador " + idJ + " efetuada com sucesso.");
                    historicosPowerUps[listPosicao].Jogador2 = partidaAtual.Jogador2;
                }
                else
                    Console.WriteLine("Falha na alteração de histórico do power Up coletado pelo jogador " + idJ);
            }

            listPos++;
            pUpActive = false;
            delayPUp = 5;
            contador = DateTime.Now;
        }

        public void pegouItemGenerico(String pego, int idJ)
        {
            Historico pegou = JsonConvert.DeserializeObject<Historico>(pego);
            WebService wSJogador = new WebService();

            if (pegou.Item.Tipo.Id_Tipo != 2)
            {
                listPos++;
                pUpActive = false;
                delayPUp = 5;
                contador = DateTime.Now;
            }

            if(pegou.Jogador1 != null || pegou.Jogador2 !=null)
            {
                return;
            }

            if (idJ == 0)
            {
                if (wSJogador.AlterarHistorico(pegou, partidaAtual.Jogador1, idJ))
                {
                    Console.WriteLine("Alteração de histórico do power Up coletado pelo jogador " + idJ + " efetuada com sucesso.");
                    pegou.Jogador1 = partidaAtual.Jogador1;
                }
                else
                    Console.WriteLine("Falha na alteração de histórico do power Up coletado pelo jogador " + idJ);
            }
            else if (idJ == 1)
            {
                if (wSJogador.AlterarHistorico(pegou, partidaAtual.Jogador2, idJ))
                {
                    Console.WriteLine("Alteração de histórico do power Up coletado pelo jogador " + idJ + " efetuada com sucesso.");
                    pegou.Jogador2 = partidaAtual.Jogador2;
                }
                else
                    Console.WriteLine("Falha na alteração de histórico do power Up coletado pelo jogador " + idJ);
            }
        }

        public void pegouApples(Historico apple, int idJ)
        {
            WebService wSJogador = new WebService();
            if (idJ == 0)
            {
                if (wSJogador.AlterarHistorico(apple, partidaAtual.Jogador1, idJ))
                {
                    Console.WriteLine("Alteração de histórico da apple coletada pelo jogador " + idJ + " efetuada com sucesso.");
                    apple.Jogador1 = partidaAtual.Jogador1;
                }
                else
                    Console.WriteLine("Falha na alteração de histórico da apple coletada pelo jogador " + idJ);
            }
            else if (idJ == 1)
            {
                if (wSJogador.AlterarHistorico(apple, partidaAtual.Jogador2, idJ))
                {
                    Console.WriteLine("Alteração de histórico da apple coletada pelo jogador " + idJ + " efetuada com sucesso.");
                    apple.Jogador2 = partidaAtual.Jogador2;
                }
                else
                    Console.WriteLine("Falha na alteração de histórico da apple coletada pelo jogador " + idJ);
            }
        }
    }
}
