﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Newtonsoft.Json;
using SnakeGame;
using Rede.Servidor.Service.WebService;

namespace Rede.Servidor
{
    class Jogador
    {
        public int id { get; set; }
        ControleJogo controleJogo;
        public TcpClient cliente { get; set; }

        StreamReader reader = null;
        StreamWriter writer = null;

        Thread thread;

        public int count { get; set; }
        public int Cod { get; set; }//----------------------
        public bool gameOver { get; set; }//----------------------
        public int maxX { get; set; }
        public int maxY { get; set; }

        public Jogador (ControleJogo controleJogo, int id, TcpClient cliente)
        {
            this.controleJogo = controleJogo;
            this.id = id;
            this.cliente = cliente;
            NetworkStream stream = cliente.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream);

            thread = new Thread(run);
            thread.Start();
            this.count = 0;
        }

        public void run ()
        {
            String dados = null;
            dados = reader.ReadLine();
            
            while (dados != null)
            {
                Console.WriteLine("Recebido do id: " + this.id + " | Mensagem: " + dados);
                if (count == 0)
                {

                    Console.WriteLine("Pegando COD, max X e max Y do jogador: " +id);
                    Console.WriteLine("a string passada é: " + dados);
                    int a = 0;

                    a = dados.IndexOf("/");
                    Cod = Int32.Parse(dados.Substring(0, a));
                    
                    Console.WriteLine("Inseriu o cod no jogador: " + id + " + " + Cod);
                    dados = dados.Substring(a + 1);
                    Console.WriteLine("a string passada é: " + dados);

                    a = dados.IndexOf("/");
                    maxX = Int32.Parse(dados.Substring(0, a));
                    Console.WriteLine("maxX é: " + maxX.ToString());
                    dados = dados.Substring(a + 1);
                    Console.WriteLine("a string passada é: " + dados);


                    maxY = Int32.Parse(dados);
                    Console.WriteLine("maxY é: " + maxY.ToString());
                    Console.WriteLine("Pegou o max X e o max Y do jogador: " + id);
                    count++;
                    dados = reader.ReadLine();
                }
                foreach (Jogador jogador in this.controleJogo.jogadores)
                {
                    if (this.id != jogador.id)
                    {
                        jogador.envia(dados);
                    }
                    else
                    {
                        
                        try
                        {
                            Player p = JsonConvert.DeserializeObject<Player>(dados);
                            gameOver = p.GameOver;
                            if (p.peguei!=null)
                            {
                                Console.WriteLine("O Jogador: " + id + " |vai avisar ao servidor que pegou um Power Up ");
                                controleJogo.pegouItemGenerico(JsonConvert.SerializeObject(p.peguei),id);
                                //controleJogo.pegouItem(controleJogo.listPos, id);
                            }
                        }
                        catch
                        {

                            //int a = 0;

                            //a = dados.IndexOf("/");
                            //if (a < 0)
                            //    a = 0;
                            //if ((dados.Substring(0, a) == "peguei o item"))
                            //{
                            //    Console.WriteLine("O Jogador: " + id + " |vai avisar ao servidor que pegou uma apple ");
                            //    dados = dados.Substring(a + 1);
                            //    Historico hTemp = JsonConvert.DeserializeObject<Historico>(dados);
                            //    foreach (Historico h in controleJogo.apples)
                            //    {
                            //        if (h == hTemp)
                            //        {
                            //            Console.WriteLine("Essa apple realmente existe e será inserida no banco");
                            //            controleJogo.pegouApples(h, id);
                            //        }
                            //    }
                            //}


                        }
                    }
                }
                dados = reader.ReadLine();
            }
        }

        public void envia (String mensagem)
        {
            writer.WriteLine(mensagem);
            writer.Flush();
            Console.WriteLine("Enviado para o id: " + this.id + " | Mensagem: " + mensagem);

        }
        public void envia(Historico h, int x, int y, int count)
        {
            string mensagem = x.ToString() + "/" + y.ToString() + "/" + count.ToString() + "/";
            mensagem += JsonConvert.SerializeObject(h);
            writer.WriteLine(mensagem);
            writer.Flush();
            Console.WriteLine("Enviado para o id: " + this.id + " | Mensagem: " + mensagem);
        }


    }
}
